import { State, Selector, Action, StateContext } from '@ngxs/store';
import { AddTest } from '../actions/test.action';

export class TestStateModel {
    schedule: any[];
}

@State<TestStateModel>({
    name: 'tests',
    defaults: {
        schedule: null
    }

})

export class OrdersState {

    constructor() { }

    @Action(AddTest)
    addTest({ setState, getState }: StateContext<TestStateModel>, { filters }: AddTest) {
        const state = getState();

        setState(Object.assign({}, state, {
            schedule: {
                // new data
            }, filters
        }));
    }

}
