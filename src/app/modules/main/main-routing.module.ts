import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            // {
            //     path: '',
            //     loadChildren: () => import('../search-worker/search-worker.module').then(m => m.SearchWorkerModule),
            //     // runGuardsAndResolvers: 'always',
            //     // canActivate: [NonWorkernGuard, NonAdminnGuard],
            //     // data: { saveSearchData: true }
            // },
            // {
            //     path: 'login',
            //     loadChildren: () => import('../user-profile/user-profile.module').then(m => m.UserProfileModule),
            // },
            // {
            //     path: 'registration',
            //     loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule),
            // }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MainRoutingModule { }
