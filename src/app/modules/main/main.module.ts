import { MainRoutingModule } from './main-routing.module';
import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [
        MainComponent,
    ],
    entryComponents: [
    ],
    imports: [
        SharedModule,
        MainRoutingModule
    ]
})

export class MainModule { }
